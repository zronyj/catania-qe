import os
import re
import math
import subprocess
from shutil import copyfile
from matplotlib import pyplot as plt

path = os.getcwd()

kp = [3, 6, 9, 12, 15, 18, 24]
dgauss = [0.00001, 0.00005, 0.0001, 0.0005, 0.001, 0.005]
celldm1 = [i/100 for i in range(280, 405, 5)]
ecutwfc = list(range(10, 65, 5))

kp0 = 2
dgauss0 = 2
celldm10 = 8
ecutwfc0 = 6

template = """&control
 calculation='relax'
 prefix='MoS2'
 outdir='./outdir/'
 pseudo_dir='/home/zronyj/Desktop/MoS2/pseudo'
 verbosity='high'
/

&system
 ibrav=12
 A={tcd1}
 B={tcd1}
 C=20.000
 cosAB=0.5
 nat=3
 ntyp=2
 ecutwfc={ecw}
 ecutrho=400
 input_dft='PBE'
 occupations='smearing'
 degauss={tdg}
 smearing='marzari-vanderbilt'
 nbnd=18
/

&electrons
 electron_maxstep=300
 mixing_beta=0.1d0
 conv_thr=1.0d-6
/

&ions
 ion_dynamics='bfgs'
/

ATOMIC_SPECIES
 Mo 95.95 Mo.pbe-spn-rrkjus_psl.1.0.0.UPF
 S 32.065 S.pbe-n-rrkjus_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
 Mo 0.000000000 0.000000000 0.5000 0 0 0
 S  0.333333333 0.333333333 0.5500 0 0 1
 S  0.666666666 0.666666666 0.4500 0 0 1

K_POINTS (automatic)
 {tkp} {tkp} 1  0 0 0
"""

pv = {"celldm1":celldm1[celldm10], "ecutwfc":ecutwfc[ecutwfc0],\
	"kp":kp[kp0], "dgauss":dgauss[dgauss0]}

name = 'MoS2_1T'

def optimizador_parametro(name, flag, entrada, options, path, pv):
	for x in options:
		pv[flag] = x
		xx_input = entrada.format(tcd1=pv["celldm1"], ecw=pv["ecutwfc"], tkp=pv["kp"], tdg=pv["dgauss"])
		wd = f'{name}-{flag}-{x}'
		cwd = os.path.join(path,wd)
		os.mkdir(cwd, 0o777)
		os.chdir(cwd)
		with open(f'{name}.in', 'w') as f:
			f.write(xx_input)
		with open(f'{name}.out', 'w') as g:
			code = subprocess.run(["mpirun", "-n", "8", "pw.x", "-i", f"{name}.in"], stdout=g)
		os.chdir(path)

def plotteador(name, flag, path):
	listos = os.listdir(path)
	xx_data = {flag:[], 'energy':[]}
	xx_date = []
	prefix = f'{name}-{flag}'
	for a in listos:
		if prefix in a:
			cwd = f'{path}/{a}/{name}.out'
			with open(cwd, 'r') as f:
				output = f.read()
			lines = re.findall(r'!.*\n', output)
			pieces = lines[-1].split()
			vaxis = float(a[len(prefix)+1:])
			xx_date.append( [float(pieces[4]), vaxis] )
	xx_date.sort(key=lambda x: x[1])
	xx_data[flag], xx_data["energy"] = list(zip(*xx_date))
	if flag == "dgauss":
		xx_data["energy"] = [math.log10(y) for y in xx_data["energy"]]
	plt.plot(xx_data["energy"], xx_data[flag], 'ro-')
	ax = plt.gca()
	ax.ticklabel_format(useOffset=False)
	plt.ticklabel_format(axis='both', style='sci', useMathText=True)
	plt.title(f'Energy vs {flag}')
	plt.xlabel(f'{flag}')
	plt.ylabel("Energy / Ry")
	plt.savefig(f'{name}-{flag}.png', bbox_inches="tight", dpi=300)
	plt.figure().clear()

# ecutwfc
optimizador_parametro(name=name, flag="ecutwfc", entrada=template,\
options=ecutwfc, path=path, pv=dict(pv))

# dgauss
optimizador_parametro(name=name, flag="dgauss", entrada=template,\
options=dgauss, path=path, pv=dict(pv))

# celldm
optimizador_parametro(name=name, flag="celldm1", entrada=template,\
options=celldm1, path=path, pv=dict(pv))

# kp
optimizador_parametro(name=name, flag="kp", entrada=template,\
options=kp, path=path, pv=dict(pv))

# plots
plotteador(name=name, flag="ecutwfc", path=path)
plotteador(name=name, flag="dgauss", path=path)
plotteador(name=name, flag="celldm1", path=path)
plotteador(name=name, flag="kp", path=path)