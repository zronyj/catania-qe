import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault
import numpy as np

plt.rcParams["figure.dpi"]=300
plt.rcParams["figure.facecolor"]="white"
plt.rcParams["figure.figsize"]=(12, 5)

fig, axs = plt.subplots(1, 2, sharex='col', sharey='row',
                        gridspec_kw=dict(width_ratios=[3,1]))


# =====> Load data

# Load the data for the band structure (check my_name.bands.gnu) <<<<<<<<<<<<<<<<<<<<<<<< CHANGE FOR YOUR CASE
data = np.loadtxt('MoS2.bands.gnu')

# Load the data for the projected density of states (check my_name.pdos.pdos_tot) <<<<<<< CHANGE FOR YOUR CASE
energy, dos, idos = np.loadtxt('1T.pdos.pdos_tot', unpack=True)

# Fermi energy (check SCF.out) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CHANGE FOR YOUR CASE
# https://mattermodeling.stackexchange.com/questions/2370/si-energy-band-values-are-not-matching-with-literature-values
fermi_energy = 0.2199

# High symmetry k-points (check Bands.out) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CHANGE FOR YOUR CASE
high_symm_kps = [
0.0000,   # Gamma
1.0184,   # K
1.5275,   # M
2.1049    # Gamma
]

# =====> Plots

# Preparing the data for the bands
k = np.unique(data[:, 0])
bands = np.reshape(data[:, 1], (-1, len(k)))

# Plot the bands
for band in range(len(bands)):
    axs[0].plot(k, bands[band, :], linewidth=1, color='r')
axs[0].axis(xmin=min(k), xmax=max(k), ymin=-6, ymax=4.5)

# Line for the Fermi energy
axs[0].axhline(fermi_energy, linestyle=(0, (5, 5)), linewidth=0.75, color='k', alpha=0.5)

# Plot each band
for symmline in high_symm_kps[1:-1]:
    axs[0].axvline(symmline, linewidth=0.75, color='k', alpha=0.5)

# text labels
axs[0].set_xticks(ticks= high_symm_kps, minor=False)
axs[0].set_xticklabels(labels=[r'$\Gamma$', 'K', 'M', r'$\Gamma$'], minor=False)
axs[0].set_ylabel("Energy (eV)")
axs[0].text(0.02, fermi_energy - 0.3, 'Fermi energy', fontsize='xx-small')

# Make horizontal plot
axs[1].plot(dos, energy, "r-", linewidth=0.8)
axs[1].axis(ymin=-6, ymax=4.5)

# Text labels
axs[1].set_xlabel('DOS')

# Line for the Fermi energy
axs[1].axhline(y=fermi_energy, linewidth=0.5, color='k', linestyle=(0, (8, 10)))
axs[1].text(-0.1, fermi_energy - 0.3, 'Fermi energy', fontsize='xx-small')

plt.show()