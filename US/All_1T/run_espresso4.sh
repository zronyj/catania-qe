#!/bin/bash

NAME='Bands'

cat > $NAME.in <<!
&control
 calculation='bands'
 prefix='MoS2'
 outdir='./outdir/'
 pseudo_dir='/home/zronyj/Desktop/MoS2/pseudo'
 verbosity='high'
/

&system
 ibrav=12
 A=3.2
 B=3.2
 C=20.000
 cosAB=0.5
 nat=3
 ntyp=2
 ecutwfc=40
 ecutrho=400
 input_dft='PBE'
 occupations='smearing'
 degauss=1.0d-3
 smearing='marzari-vanderbilt'
 nbnd=18
/

&electrons
 electron_maxstep=100
 mixing_beta=0.1d0
 conv_thr=1.0d-6
/

&ions
 ion_dynamics='bfgs'
/

ATOMIC_SPECIES
 Mo 95.95 Mo.pbe-spn-rrkjus_psl.1.0.0.UPF
 S 32.065 S.pbe-n-rrkjus_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
Mo            0.0000000000        0.0000000000        0.5000000000    0   0   0
S             0.3333333330        0.3333333330        0.5790839078    0   0   1
S             0.6666666660        0.6666666660        0.4209160922    0   0   1

K_POINTS crystal_b
 4
 0.000000000 0.000000000 0.000000000 50 !G
 0.333333333 0.666666666 0.000000000 50 !K
 0.000000000 0.500000000 0.000000000 50 !M
 0.000000000 0.000000000 0.000000000 50 !G
!

mpirun -n 8 pw.x -i $NAME.in > $NAME.out