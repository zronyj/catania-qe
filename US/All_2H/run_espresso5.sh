#!/bin/bash

NAME='Bands2'

cat > $NAME.in <<!
&bands
 prefix='MoS2'
 outdir='./outdir/'
 filband='MoS2.bands'
/
!

mpirun -n 8 bands.x < $NAME.in > $NAME.out